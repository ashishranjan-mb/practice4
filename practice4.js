/*

   Q. Read the result.json and gather data of all the people who failed in both their exams.
        Less than 35 score is failed.
   Q. Gather data of students who has failed in only computer science.

   Q. Calculate average for each student and save it to a new file

   Q. Sort data with student name and sort the subjects array on alphabetical order as well.

   Q. Generate result of user in this form and write it to a new file.
        ComputerScience - computersciencemarks , Mathematics - mathematicsMarks : studentName

        Display result in each separate line.

*/

const fs = require('fs')
const data = require('./result.json')

//PROBLEM 1
const jsonData = Object.entries(data);
console.log(jsonData.filter((item)=>{
let mathMarks = item[1][0]['Mathematics'];
let csMarks =  item[1][0]['Computer Science'];
if(mathMarks < 35 && csMarks < 35){
     return item;
}
}));

//PROBLEM 2
const jsonData = Object.entries(data);
console.log(jsonData.filter((item)=>{
    let mathMarks = item[1][0]['Mathematics'];
    let csMarks =  item[1][0]['Computer Science'];
    if(csMarks < 35 && mathMarks >= 35){
         return item
    }
}));

//PROBLEM 3
const jsonData = Object.entries(data);
 let avg = jsonData.map((item)=>{
    let mathMarks = item[1][0]['Mathematics'];
    let csMarks =  item[1][0]['Computer Science'];
    let avgMarks = (mathMarks+csMarks)/2;
    const newObj = {};
    newObj[item[0]] = avgMarks;
    return newObj;

});
console.log(avg);
fs.writeFile('./avgMarks.json', JSON.stringify(avg),(err)=>{
    console.log(err)});



//PROBLEM 4
const jsonData = Object.entries(data);
let sortedArray= jsonData.sort((a,b)=>{
    if (a[0] < b[0]) {
         return -1;
       }
       if (a[0] > b[0]) {
         return 1;
       }
       return 0;

});

console.log(sortedArray);


//PROBLEM 5
const jsonData = Object.entries(data);
let resultData = jsonData.map((item)=>{
    let mathMarks = item[1][0]['Mathematics'];
    let csMarks = item[1][0]['Computer Science'];
    let name = item[0];
    return `ComputerScience - ${csMarks} , Mathematics - ${mathMarks} : ${name}`;
});
console.log(resultData);
fs.writeFile('./resultData.json',JSON.stringify(resultData),(err)=>{
    console.log(err)});

